<?php
/**
 * Created by PhpStorm.
 * User: pekowal
 * Date: 15.08.2018
 * Time: 12:21
 */

?>

<!doctype html>
<html lang="en">


<?php
include('head.html');
?>

<body>

<style>

</style>


<div class="container">
    <div class="mt-5">
        <h3>Simple comparision loading sites</h3>
    </div>
    <div class="d-flex justify-content-center mt-5">
        <form style="width: 100%" id="benchmark_form" action="results.php" method="post">
            <div class="form-group input-group">


                <input class="form-control" type="text" name="main_site" placeholder="Main site">
            </div>
            <div class="d-flex justify-content-between">
                <button type="submit" class="btn btn-success">Compare sites!</button>
                <button id="add-site" type="button" class="btn btn-primary">
                    <i class="fas fa-plus"></i>
                </button>
            </div>

        </form>

    </div>
</div>


<script>

    function CheckIsValidDomain(domain) {
        var re = new RegExp(/^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/);
        return domain.match(re);
    }

    $(function () {

        let counterAddSites = 1;

        $('body').on('click', '.delete-site', function () {

            $(this).closest('.input-group').remove();
        });

        $('#add-site').click(function () {


            let additionalInput = $('<div class="input-group mb-3">\n' +
                '  <input type="text" class="form-control" name="add_site_' + counterAddSites + '" placeholder="Additional site" aria-label="Additional site" aria-describedby="basic-addon2">\n' +
                '  <div class="input-group-append">\n' +
                '    <button class="btn btn-outline-secondary delete-site text-danger" type="button"><i class="fas fa-minus"></i></button>\n' +
                '  </div>\n' +
                '</div>');

            $('#benchmark_form .input-group').last().after(additionalInput);
            counterAddSites++;

        });


    });
</script>

</body>

</html>