<?php

require('Benchmark/SiteTest.php');
require('Benchmark/SiegeManager.php');
require('Service/ApiSmsService.php');

$siegeManager = new \Benchmark\SiegeManager(new \Service\ApiSmsService('key', 'secret'));

$sitesTests = $siegeManager->testWithNotification($_POST);

$siegeManager->createLogFile($sitesTests);


?>
<!doctype html>
<html lang="en">


<?php
include('head.html');
?>

</head>


<div class="container">
    <div class="row">
        <div class="col-12 d-flex justify-content-between">
            <?php

            $place = 1;
            foreach ($sitesTests as $sitesTest) {
                echo "    
        <div class=\"card\" style=\"width: 18rem;\">
        <div class=\"card-body\">
        <div class='d-flex justify-content-center mb-5 mt-5' style='font-size: 20px'>#".$place."</div>
            <h5 class=\"card-title\">" . $sitesTest->getUri() . "</h5>
            <p class=\"card-text\">Response time: " . $sitesTest->getResponseTime() . " secs</p>
            ";
                echo "<p class=\"card-text\">Elasped time: " . $sitesTest->getElapsedTime() . " secs</p>";
                echo "<p class=\"card-text\">Transaction rate: " . $sitesTest->getTransactionRate() . " trans/sec</p>";

                foreach ($sitesTest->getCompareData() as $compareData){
                    echo " <p class=\"card-text\">" . $compareData . "</p>";
                }

                echo "
        </div>
    </div>";
                $place++;
            }

            ?>
        </div>

    </div>
</div>


<body>

</body>

</html>

