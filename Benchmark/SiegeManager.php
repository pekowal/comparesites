<?php
/**
 * Created by PhpStorm.
 * User: pekowal
 * Date: 15.08.2018
 * Time: 10:59
 */

namespace Benchmark;


//include ('./../Test/SiteTest.php');

use Service\ApiSmsService;

class SiegeManager
{

    private $smsService;

    public function __construct(ApiSmsService $smsService)
    {
        $this->smsService = $smsService;
    }

    private static function compareByResponseTime(SiteTest $a, SiteTest $b)
    {
        return strcmp($a->getResponseTime(), $b->getResponseTime());

    }


    public function testWithNotification(array $arraySites)
    {

        $siteTests = $this->testPages($arraySites);

        unset($arraySites[0]);

        foreach ($siteTests as $siteTest) {

            $this->sendWarningEmail($siteTest);

        }

        foreach ($this->fetchCriticalTests($siteTests) as $criticalTest){

            $this->smsService->sendWarningMessage($criticalTest);
        }

        return $siteTests;
    }

    public function testPages(array $arraySites)
    {
        $siteTests = [];

        foreach ($arraySites as $uri) {
            $siteTests[] = $this->testPage($uri);
        }

        $siteTests = $this->compareWithOthers($siteTests);

        return $siteTests;
    }

    public function testPage(string $uri): SiteTest
    {
        exec('siege -c30 -r1 ' . $uri . ' 2>&1', $output);

        $siteTest = new SiteTest($uri);
        $siteTest->setUri($uri);
        foreach ($output as $item) {


            if (strpos($item, 'Elapsed time') !== false) {

                $siteTest->setElapsedTime(preg_replace("/[^0-9\.]/", '', $item));
            }

            if (strpos($item, 'Response time') !== false) {
                $siteTest->setResponseTime(floatval(preg_replace("/[^0-9\.]/", '', $item)));


            }
            if (strpos($item, 'Transaction rate') !== false) {
                $siteTest->setTransactionRate(preg_replace("/[^0-9\.]/", '', $item));

            }
        }


        return $siteTest;

    }

    public function createLogFile(array $tests): bool
    {

        usort($tests, array($this, 'compareByResponseTime'));

        $output = '';
        $outputErrors = '';

        $places = 1;

        for ($i = 1; $i <= count($tests); $i++) {

            $usedTest = $tests[$i - 1];
            if ($usedTest->getResponseTime() > 0) {
                $output .= 'PLACE #' . $places . PHP_EOL;
                $output .= 'Response time = ' . $usedTest->getResponseTime() .' sec'. PHP_EOL;
                $output .= 'Elasped time = ' . $usedTest->getElapsedTime() . ' sec'. PHP_EOL;
                $output .= 'Transaction rate = ' . $usedTest->getTransactionRate() .' trans/sec'.PHP_EOL;


                foreach ($usedTest->getCompareData() as $compareData){
                    $output .=  $compareData . PHP_EOL;
                }

                $places++;
            } else {
                $outputErrors .= 'INVALID TEST REQUEST WITHOUR RESPONSE #' . PHP_EOL;
                $outputErrors .= 'Response time = ' . $usedTest->getResponseTime() . PHP_EOL;

            }

        }

        $my_file = 'logs/log_' . md5(time()) . '.txt';
        $handle = fopen($my_file, 'w') or die('Cannot open file:  ' . $my_file);
        fwrite($handle, 'Date '.date('Y-m-d H:i:s'.PHP_EOL));
        fwrite($handle, $output);
        fwrite($handle, $outputErrors);
        return true;

    }


    private function compareWithOthers(array $siteTests){

        foreach ($siteTests as $siteTest) {

            $compareData = [];
            foreach ($siteTests as $cmpTest) {

                if ($siteTest != $cmpTest) {

                    if ($cmpTest->getResponseTime() > $siteTest->getResponseTime()){
                        $speedRatio = round($cmpTest->getResponseTime() / $siteTest->getResponseTime(),2);
                        $compareData[] = $speedRatio.'x faster than '.$cmpTest->getUri();

                    }else{
                        $speedRatio = round($siteTest->getResponseTime() / $cmpTest->getResponseTime(),2);
                        $compareData[] = $speedRatio.'x slower than '.$cmpTest->getUri();

                    }

                }

            }
            $siteTest->setCompareData($compareData);

        }

        return $siteTests;
    }

    private function fetchCriticalTests(array $siteTests)
    {

        array_reverse($siteTests);
        $toReturn = [];

        foreach ($siteTests as $siteTest) {

            if ($siteTest->getResponseTime() == 0){
                $toReturn[] = $siteTest;
                continue;
            }

            foreach ($siteTests as $cmpTest) {

                if ($siteTest != $cmpTest && $cmpTest->getResponseTime() * 2 < $siteTest->getResponseTime()) {

                    $toReturn[] = $siteTest;
                    continue;

                }

            }

        }

        return $toReturn;
    }

    //TODO INPUT FOR EMAIL
    private function sendWarningEmail($email)
    {

        $to = 'nobody@example.com';
        $subject = 'the subject';
        $message = 'hello';
        $headers = array(
            'From' => 'webmaster@example.com',
            'Reply-To' => 'webmaster@example.com',
            'X-Mailer' => 'PHP/' . phpversion()
        );

        mail($to, $subject, $message, $headers);
    }


}