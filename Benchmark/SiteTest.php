<?php
/**
 * Created by PhpStorm.
 * User: pekowal
 * Date: 15.08.2018
 * Time: 14:18
 */
namespace Benchmark;


class SiteTest
{

    private $uri;
    private $elapsedTime;
    private $responseTime;
    private $transactionRate;
    private $compareData;
//    private $loadingTime;

    public function __construct($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return mixed
     */
    public function getResponseTime()
    {
        return $this->responseTime;
    }

    /**
     * @param mixed $responseTime
     */
    public function setResponseTime($responseTime)
    {
        if ($responseTime == 0){

        }
        $this->responseTime = $responseTime;
    }

    /**
     * @return mixed
     */
    public function getTransactionRate()
    {
        return $this->transactionRate;
    }

    /**
     * @param mixed $transactionRate
     */
    public function setTransactionRate($transactionRate)
    {
        $this->transactionRate = $transactionRate;
    }

    /**
     * @return mixed
     */
    public function getElapsedTime()
    {
        return $this->elapsedTime;
    }

    /**
     * @param mixed $elapsedTime
     */
    public function setElapsedTime($elapsedTime)
    {
        $this->elapsedTime = $elapsedTime;
    }

    /**
     * @return mixed
     */
    public function getCompareData()
    {
        return $this->compareData;
    }

    /**
     * @param mixed $compareData
     */
    public function setCompareData($compareData): void
    {
        $this->compareData = $compareData;
    }


}